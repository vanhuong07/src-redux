import React, { Component } from "react";

class ReactForm extends Component {
  state = {
    user: {
      userName: "",
      fullName: "",
      email: "",
      phone: "",
      type: "",
    },
  };

  handleChange = (e) => {
    // console.log("input change", e.target.value, e.target.name);
    this.setState({
      user: { ...this.state.user, [e.target.name]: e.target.value },
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.user);
  }

  handleSetUpdate = () => {
    const user = {
      userName: "a",
      fullName: "b",
      email: "c",
      phone: "@",
      type: "VIP",
    };
    this.setState({
      user: user,
    });
  }


  render() {
    return (
      <form className="w-50 mx-auto" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>Username</label>
          <input
            onChange={this.handleChange}
            name="userName"
            value={this.state.user.userName}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Name</label>
          <input
            onChange={this.handleChange}
            name="fullName"
            value={this.state.user.fullName}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Email</label>
          <input
            onChange={this.handleChange}
            name="email"
            value={this.state.user.email}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Phone Number</label>
          <input
            onChange={this.handleChange}
            name="phone"
            value={this.state.user.phone}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Type</label>
          <select
            onChange={this.handleChange}
            name="type"
            value={this.state.user.type}
            className="form-control"
          >
            <option>USER</option>
            <option>VIP</option>
          </select>
        </div>
        <button type="submit" className="btn btn-success">
          Submit
        </button>
        <button type="button" onClick={this.handleSetUpdate} className="btn btn-info ml-3">Set Value</button>
      </form>
    );
  }
}

export default ReactForm;

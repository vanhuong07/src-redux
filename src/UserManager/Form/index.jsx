import React, { Component } from "react";
class Modal extends Component {
  render() {
    return (
      <div
        style={{
          background: "rgba(0,0,0,0.7)",
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          display: "flex",
          alignItems: "center",
        }}
      >
        <div className="bg-white w-50 mx-auto px-5 pb-3 rounded ">
          <h1 className="text-center display-4 m-0">Form User</h1>
          <form className="text-white" onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>Username</label>
              <input type="text" className="form-control" />
            </div>
            <div className="form-group">
              <label>Name</label>
              <input type="text" className="form-control" />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input type="text" className="form-control" />
            </div>
            <div className="form-group">
              <label>Phone Number</label>
              <input type="text" className="form-control" />
            </div>
            <div className="form-group">
              <label>Type</label>
              <select className="form-control">
                <option>USER</option> <option>VIP</option>
              </select>
            </div>
            <button type="submit" className="btn btn-success">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Modal;

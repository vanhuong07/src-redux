import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore , combineReducers } from 'redux';
import {Provider} from 'react-redux';
import formReducer from './UserManager/Redux/reducer/form';
import userList from './UserManager/Redux/reducer/userlist';
//tạo reducer tổng (root)

const reducer = combineReducers ({
  isShow: formReducer,
  userList,
});
// const reducer = combineReducers({
//     //nơi lưu dữ liệu của store
//   // tenDuLieu: reducerQuanLy
//   isShow: formReducer,
// });

const store = createStore(reducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
